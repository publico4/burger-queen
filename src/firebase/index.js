import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyATFrdN5eifSgWP6NSnN3QxHEwcgyOiJqc",
  authDomain: "proyecto-react-4ca9a.firebaseapp.com",
  projectId: "proyecto-react-4ca9a",
  storageBucket: "proyecto-react-4ca9a.appspot.com",
  messagingSenderId: "745662322884",
  appId: "1:745662322884:web:690994b9fdfefc6e0dd191",
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export { db, firebase };
