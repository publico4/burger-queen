export const menu = {
  desayuno: [
    {
      name: "CAFÉ AMERICANO",
      price: 5,
    },
    {
      name: "CAFÉ CON LECHE",
      price: 7,
    },
    {
      name: "SANDWICH DE JAMON Y QESO",
      price: 10,
    },
    {
      name: "JUGO NARUTAL",
      price: 7,
    },
  ],
  almuerzo: [
    {
      name: "LOMO SALTADO",
      price: 25,
    },
    {
      name: "ARROZ CON POLLO",
      price: 17,
    },
    {
      name: "CUY FRITO",
      price: 30,
    },
    {
      name: "LOMO A LO POBRE",
      price: 27,
    },
  ],
};
